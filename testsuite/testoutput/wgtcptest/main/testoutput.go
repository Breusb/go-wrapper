package main

import (
	"fmt"
	"net"
	"wrapper/lib"
)

func main() {
	conf, err := wgtcp.Configurate("../../config-wireguard.json")
	wgtcp.HandleError(err)

	addr := wgtcp.ResolveAddrStr(conf.WGClient.IP, conf.WGClient.UDPPort)
	UDPAddr, err := net.ResolveUDPAddr("udp4", addr)
	wgtcp.HandleError(err)

	UDPConn, err := net.ListenUDP("udp4", UDPAddr)
	wgtcp.HandleError(err)
	fmt.Printf("UDP listener set-up on %s", UDPAddr)

	defer UDPConn.Close()

	buf := make([]byte, 1900)

	for {
		n, _, err := UDPConn.ReadFromUDP(buf)
		wgtcp.HandleError(err)
		fmt.Printf("Packets received with %d bytes on %q UDP \n", n, UDPAddr)
	}
}
