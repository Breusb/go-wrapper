// Main package to check the server operating mode of wgtcp
package main

import (
	"fmt"
	"net"
	"wrapper/lib"
)

func main() {
	// Set-up configuration to listen for test output
	conf, err := wgtcp.Configurate("../config-server.json")
	wgtcp.HandleError(err)

	// Resolve portnumber and set up TCP listener on the port
	listenStr := wgtcp.ResolveAddrStr("", conf.Local.TCPPort)
	TCPAddr, err := net.ResolveTCPAddr("tcp4", listenStr)
	wgtcp.HandleError(err)

	TCPListener, err := net.ListenTCP("tcp4", TCPAddr)
	wgtcp.HandleError(err)

	TCPConn, err := TCPListener.AcceptTCP()
	wgtcp.HandleError(err)

	defer TCPConn.Close()

	buf := make([]byte, 1980)

	for {
		n, err := TCPConn.Read(buf)
		wgtcp.HandleError(err)
		fmt.Printf("Packet-received: bytes=%d on TCP port %d \n", n, conf.Local.TCPPort)
	}
}
