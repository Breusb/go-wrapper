// Package main sets up the testinput which acts as WireGuard client.
package main

import (
	"fmt"
	"math/rand"
	"net"
	"time"
	"wrapper/lib"
)

func main() {
	// Set-up configuration for where to send test input
	conf, err := wgtcp.Configurate("../config-client.json")
	wgtcp.HandleError(err)

	// Resolve address of the WireGuard client (claddr)
	sendStr := wgtcp.ResolveAddrStr(conf.Local.IP, conf.Local.UDPPort)
	claddr, err := net.ResolveUDPAddr("udp4", sendStr)
	wgtcp.HandleError(err)

	// Resolve the local address
	lStr := wgtcp.ResolveAddrStr(conf.Local.IP, 12345)
	laddr, err := net.ResolveUDPAddr("udp4", lStr)
	wgtcp.HandleError(err)

	UDPConn, err := net.DialUDP("udp4", laddr, claddr)
	wgtcp.HandleError(err)

	defer UDPConn.Close()

	i := 0

	for {
		var randInt int
		randInt = rand.Intn(1200)
		buf := make([]byte, randInt)
		_, err := rand.Read(buf)
		if err != nil {
			fmt.Println(buf, err)
		}
		i++
		n, err := UDPConn.Write(buf)
		if err != nil {
			fmt.Println(buf, err)
		}
		fmt.Printf("Packets written: bytes=%d on connection to %q for message %d UDP \n", n, claddr, i)
		time.Sleep(time.Second * 2)
	}
}
