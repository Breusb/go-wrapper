# WG-TCP

 WG-TCP is a program which enables WireGuard Go Client users to use WireGuard over TCP besides UDP.
 Install the program on both sides (server/client) of the VPN connection to work.
 Currently, it is only ment for a server/client VPN set-up and is not tested with peer-to-peer.

## Source Structure
```shell   
wrapper/                          # → Root dir
├── lib/                          # → Library dir
│   ├── wgtcp.go                  # → Shared functions
├── wgTCP/                        # → Main dir
│   ├── main/                     # → Main package for wgTCP
|       ├── main.go               # → Main GO file - wgTCP source
│   ├── config-client.json        # → Config for client mode
│   ├── config-server.json        # → Config for server mode
├── testsuite/                    # → Testsuite dir
│   ├── testinput/                # → Main package for testinput
|     	├── ...			  # → Testinput source
│   ├── testoutput/               # → Test output checker
|     	├── ...			  # → Testinput source
		
## TODO
- [ ] Test with WireGuard Go client
- [ ] Recoverable errors
- [ ] Start dialing after user confirms listener on other device is set up

## Coming features
- [ ] Peer-to-peer support (Go-client)
- [ ] Client/server support (Go-client)
- [ ] Peer-to-peer support (native WG)
- [ ] Client/server support (native WG)
- [ ] IP change recovery
- [ ] MTU path discovery
