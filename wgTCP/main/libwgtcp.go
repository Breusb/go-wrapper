//Package wgtcp consists of the main functionality of the WG-TCP program
package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"time"
)

// Config structure defintion
type Config struct {
	Local struct {
		IP      string `json:"IP"`
		UDPPort int    `json:"UDPPort"`
		TCPPort int    `json:"TCPPort"`
	} `json:"Local"`
	Remote struct {
		IP      string `json:"IP"`
		TCPPort int    `json:"TCPPort"`
	} `json:"Remote"`
	WGClient struct {
		IP      string `json:"IP"`
		UDPPort int    `json:"UDPPort"`
	} `json:"WGClient"`
}

// Configurate retrieve config from the JSON file and returns it as a struct
func Configurate(filename string) (conf *Config, err error) {
	//Open the JSON config file
	confFile, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	//Close the file after the function pops
	defer confFile.Close()

	//Decode the config file
	decoder := json.NewDecoder(confFile)
	err = decoder.Decode(&conf)
	if err != nil {
		return nil, err
	}

	return conf, nil
}

// Mode checks the mode argument given when launching the application
func Mode() int {
	if len(os.Args) < 2 {
		return 1
	}
	var modeArg = os.Args[1]
	if modeArg == "-c" || modeArg == "--client" {
		return 2
	} else if modeArg == "-s" || modeArg == "--server" {
		return 3
	} else if modeArg == "-p" || modeArg == "--peertopeer" {
		return 1
	}
	return 0
}

// ConfigFile checks the cnfiguration file argument given when launching the application
func ConfigFile() (confArg string) {
	if len(os.Args) == 0 || len(os.Args) == 1 && os.Args[1][0] == '-' {
		return "notspecified"
	} else if len(os.Args) == 1 && os.Args[1][0] != '-' || len(os.Args) > 1 && os.Args[2][0] == '-' {
		return "firstarg"
	} else {
		return "secondarg"
	}
}

// WaitEnter is used to wait for the user to give an enter as input before going on.
func WaitEnter(IP string, port int) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Press enter as soon as the port %d is opened on the target machine %q", port, IP)
	reader.ReadString('\n')
}

// HandleError is used to handle errors in the default, but fatal, way.
func HandleError(err error) {
	if err != nil {
		log.Fatal(err, "\n")
	}
}

/*
ResolveAddrStr resolves an IP addr string from given port (int) and IP (string)
"" (IP), 0 (port) results  in : (string)
"" (IP), x (port) results in :x (string)
x (IP), 0 (port) results in IP:* (string)
x (IP), x (port) resul5555ts in IP:x (string )
*/
func ResolveAddrStr(IP string, port int) (AddrStr string) {
	if IP == "" {
		if port == -1 {
			AddrStr = string(":*")
		} else {
			AddrStr = string(':') + strconv.Itoa(port)
		}
	} else {
		if port == -1 {
			AddrStr = IP + string(":*")
		} else {
			AddrStr = IP + string(':') + strconv.Itoa(port)
		}
	}

	return AddrStr
}

// UDPtoTCP is used to set up a UDP listener, reads its content, and writes all data on a TCP connection.
func UDPtoTCP(conf *Config) (err error) {

	// Translate portnumber (int) into UDPAddr (UDPAddr).
	listenStr := ResolveAddrStr("", conf.Local.UDPPort)
	UDPAddr, err := net.ResolveUDPAddr("udp4", listenStr)
	if err != nil {
		return err
	}

	// Set up listener on specified UDP port
	UDPConn, err := net.ListenUDP("udp4", UDPAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Set up UDP Listener on %q \n", UDPAddr)

	defer UDPConn.Close()

	// Resolve the TCPAddr from the configuration parameter (conf)
	remoteStr := ResolveAddrStr(conf.Remote.IP, conf.Remote.TCPPort)
	TCPAddr, err := net.ResolveTCPAddr("tcp4", remoteStr)
	if err != nil {
		return err
	}

	WaitEnter(conf.Remote.IP, conf.Remote.TCPPort)

	// Set up a TCP dialer for the remote address
	TCPConn, err := net.DialTimeout("tcp", remoteStr, 20*time.Second)
	if err != nil {
		return err
	}

	fmt.Printf("Dialing %q via TCP succeeded \n", TCPAddr)

	// When the function finsihes, close the established TCP connection
	defer TCPConn.Close()

	// Create temp. buffers to store the UDP packet content in
	buf := make([]byte, 1500)

	// Keep listening and reading from the UDP Connection unto the buffer, and send this buffer via TCP
	for {
		n, _, err := UDPConn.ReadFromUDP(buf)
		if err != nil {
			return err
		}
		fmt.Printf("Message of %d bytes received on UDP from %q", n, UDPAddr)

		n, err = TCPConn.Write(buf[0:n])
		if err != nil {
			return err
		}
		fmt.Printf("Send message of %d bytes to %q", n, TCPAddr)

	}
}

// TCPtoUDP Set up a TCP listener on a port, writes all data read on a UDP Connection
func TCPtoUDP(conf *Config) (err error) {

	// Resolve portnumber (int) into TCPAddr (TCPAddr)
	listenStr := ResolveAddrStr("", conf.Local.TCPPort)
	TCPAddr, err := net.ResolveTCPAddr("tcp4", listenStr)
	if err != nil {
		return err
	}

	// Sry againet up listener on specified TCP port
	TCPListener, err := net.ListenTCP("tcp4", TCPAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Set up TCP Listener on %q \n", TCPAddr)

	// Accept incomming connection on the TCP port
	TCPConn, err := TCPListener.AcceptTCP()
	if err != nil {
		return err
	}

	// When the function finishes, close the established UDP connection
	defer TCPConn.Close()

	// Resolve the UDPAddr from the configuration
	remoteStr := ResolveAddrStr(conf.WGClient.IP, conf.WGClient.UDPPort)
	UDPAddr, err := net.ResolveUDPAddr("udp4", remoteStr)
	if err != nil {
		return err
	}

	WaitEnter(conf.WGClient.IP, conf.WGClient.UDPPort)

	// Set up UDP connection to the WireGuard address
	UDPConn, err := net.DialUDP("udp4", nil, UDPAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Dialing %q via UDP succeeded \n", UDPAddr)

	defer UDPConn.Close()

	// Set up temp buffer to store the incomming TCP traffic
	buf := make([]byte, 1500)

	for {
		//Read from connection
		n, err := TCPConn.Read(buf)
		if err != nil {
			return err
		}
		fmt.Printf("Message recieved of %d bytes from %q", n, TCPAddr)

		//Send it unto the UDP connection
		_, err = UDPConn.Write(buf[0:n])
		if err != nil {
			return err
		}
	}
}
