//Package contain the TCPwrapper set-up for the clientside of the WireGuard VPN
package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
)

func main() {
	var configPath string
	switch ConfigFile() {
	case "notspecified":
		fmt.Println("Default configuration file used")
		configPath = "../wgctp-config-client.json"
	case "firstarg":
		configPath = os.Args[1]
	case "secondarg":
		configPath = os.Args[2]
	}

	fmt.Printf("Config file used is: %q \n", configPath)
	conf, err := Configurate(configPath)
	HandleError(err)
	switch Mode() {
	case 1: // Peer-to-peer
		var wg sync.WaitGroup

		wg.Add(2)
		go func() {
			err = UDPtoTCP(conf)
			fmt.Printf("%q", err)
			defer wg.Done()
		}()

		go func() {
			err = TCPtoUDP(conf)
			fmt.Printf("%q", err)
			defer wg.Done()
		}()

		wg.Wait()
		fmt.Println("WG-TCP has stopped working in p2p mode")
	case 2: // Client
		ctx := context.Background()
		err = setUpClient(ctx, conf)
		if err != nil {
			log.Fatal(err)
		}

	case 3: // Server
		ctx := context.Background()
		err := setUpServer(ctx, conf)
		if err != nil {
			log.Fatal(err)
		}
	case 0: // Error
		log.Fatal("The given operating mode is not correct, use: --client (-c), --server (-s) or --peertopeer (-p) \n")
	}
}
