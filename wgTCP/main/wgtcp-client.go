package main

import (
	"context"
	"fmt"
	"net"
)

func handleClientToTCPUDP(c *net.TCPConn, pc *net.UDPConn, ch chan error) (err error) {
	for {
		buf := make([]byte, 2000)
		oob := make([]byte, 2000)

		n, oobn, flags, addr, err := pc.ReadMsgUDP(buf, oob)

		if err != nil {
			ch <- err
			return err
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes copied from %q with flags: %q and the out-of-band data: %q", n, addr, flags, oobn)
		}
	}
}

/*
 * handleTCPToClient handles the entire road from the local WG client to the remote WG-TCP
 */
func handleClientToTCP_c(c *net.TCPConn, pc *net.UDPConn, ch chan error) (err error) {
	for {
		// Buffer to hold the UDP packets
		buf := make([]byte, 2000)

		n, addr, err := pc.ReadFromUDP(buf)
		if err != nil {
			fmt.Println("Something went wrong")
			ch <- err
			return err
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes read from %q \n", n, addr)
			//fmt.Println(buf[:n])
			k, err := c.Write(buf[:n])
			if err != nil {
				ch <- err
				return err
			}
			fmt.Printf("%d bytes sent to remote WG-TCP %q \n", k, c.RemoteAddr())
			//fmt.Println(buf[:k])
		}
	}
}

/*
 * handleClientToTCP handles the entire road from the remote WG-TCP to the local WG client
 */
func handleTCPtoClient_c(c *net.TCPConn, pc *net.UDPConn, ch chan error) (err error) {
	for {
		// Buffer to read the incomming TCP packets to
		buf := make([]byte, 2000)
		n, err := c.Read(buf)
		fmt.Printf("%d bytes read from remote WG-TCP %q \n", n, c.RemoteAddr())
		if err != nil {
			ch <- err
			return err
		} else if err == nil && n > 0 {
			k, err := pc.Write(buf[:n])
			if err != nil {
				ch <- err
				return err
			}
			fmt.Printf("%d bytes written to local WG-Client %q \n", k, pc.RemoteAddr())
		}
	}
}

/*
 * setUpClient handles the set up for the WG-TCP on the client side of the VPN
 */
func setUpClient(ctx context.Context, conf *Config) (err error) {

	// Set up UDP listener on the port which the WG Client uses as endpoint
	wgStrRet := ResolveAddrStr(conf.Local.IP, conf.Local.UDPPort)
	udpRetAddr, err := net.ResolveUDPAddr("udp4", wgStrRet)
	udpRetConn, err := net.ListenUDP("udp4", udpRetAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Listening on (UDP) %q \n", udpRetAddr)
	defer udpRetConn.Close()

	// Set up UDP connection with the WG Client
	// wgStr := ResolveAddrStr(conf.WGClient.IP, conf.WGClient.UDPPort)
	// udpWGAddr, err := net.ResolveUDPAddr("udp4", wgStr)
	//
	// udpWGConn, err := net.DialUDP("udp4", udpRetAddr, udpWGAddr)
	// if err != nil {
	// 	return err
	// }
	// fmt.Printf("Dialed (UDP) %q \n", udpWGConn.RemoteAddr())
	// defer udpWGConn.Close()

	// Set up TCP connection with the VPN server WG-TCP wrapper
	remoteStr := ResolveAddrStr(conf.Remote.IP, conf.Remote.TCPPort)
	tcpRemoteAddr, err := net.ResolveTCPAddr("tcp4", remoteStr)
	localStr := ResolveAddrStr("", conf.Local.TCPPort)
	tcpLocalAddr, err := net.ResolveTCPAddr("tcp4", localStr)
	tcpRemoteConn, err := net.DialTCP("tcp4", tcpLocalAddr, tcpRemoteAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Dialed (TCP) %q \n", tcpRemoteConn.RemoteAddr())
	defer tcpRemoteConn.Close()

	// Make a done channel for error messages
	doneCh := make(chan error, 1)

	// Handle the connection from the WG Client to the remote WG-TCP
	go handleTCPtoClient_c(tcpRemoteConn, udpRetConn, doneCh)

	// Handle the connection from the remote WG-TCP to the WG Client
	go handleClientToTCP_c(tcpRemoteConn, udpRetConn, doneCh)

	// Handle termination
	var input string
	fmt.Scanln(&input)
	return

}
