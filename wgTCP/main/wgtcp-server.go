package main

import (
	"context"
	"fmt"
	"net"
	"strconv"
)

func handleTCPtoClient_s(c *net.TCPConn, pc *net.UDPConn) (err error) {
	fmt.Printf("Serving %s\n", c.RemoteAddr().String())
	for {
		buf := make([]byte, 2000)
		n, err := c.Read(buf)
		fmt.Printf("%d bytes read from remote WG-TCP %q \n", n, c.RemoteAddr())
		if err != nil {
			return err
		} else if err == nil && n > 0 {
			//wgStr, _ := net.ResolveUDPAddr("127.0.0.1", "51820")
			pc.WriteTo(buf[:n], nil)
			fmt.Printf("%d bytes written to local WG-Client %q \n", n, pc.RemoteAddr())
		}
		n, addr, err := pc.ReadFromUDP(buf)
		if err != nil {
			return err
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes read (UDP) from %q \n", n, addr)
			//fmt.Println(buf[:n])
			k, err := c.Write(buf[:n])
			if err != nil {
				return err
			}
			fmt.Printf("%d bytes sent to remote WG-TCP %q \n", k, c.RemoteAddr())
			//fmt.Println(buf[:k])
		}
	}
}

func handleClientToTCP_s(c *net.TCPConn, pc *net.UDPConn) (err error) {
	for {
		buf := make([]byte, 2000)
		n, addr, err := pc.ReadFromUDP(buf)
		if err != nil {
			return err
		} else if err == nil && n > 0 {
			fmt.Printf("%d bytes read (UDP) from %q \n", n, addr)
			//fmt.Println(buf[:n])
			k, err := c.Write(buf[:n])
			if err != nil {
				return err
			}
			fmt.Printf("%d bytes sent to remote WG-TCP %q \n", k, c.RemoteAddr())
			//fmt.Println(buf[:k])
		}

	}
}

/*
 * setUpServer handles the set up for the WG-TCP on the server side of the VPN
 */
func setUpServer(ctx context.Context, conf *Config) (err error) {

	// Set up TCP listener on the port which the remote WG-TCP will connect to
	portStr := ":" + strconv.Itoa(conf.Local.TCPPort)
	tcpPortAddr, err := net.ResolveTCPAddr("tcp4", portStr)
	tcpPortListener, err := net.ListenTCP("tcp4", tcpPortAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Listening on (TCP) %q \n", tcpPortAddr)
	defer tcpPortListener.Close()

	// // Set up UDP listener on the port which the WG Client uses as endpoint
	// wgStrRet := ResolveAddrStr(conf.Local.IP, conf.Local.UDPPort)
	// udpRetAddr, err := net.ResolveUDPAddr("udp4", wgStrRet)
	// udpRetConn, err := net.ListenUDP("udp4", udpRetAddr)
	// if err != nil {
	// 	return err
	// }
	// fmt.Printf("Listening on (UDP) %q \n", udpRetAddr)
	// defer udpRetConn.Close()

	// Set up UDP connection with the WG Client
	wgStr := ResolveAddrStr(conf.WGClient.IP, conf.WGClient.UDPPort)
	wgStrRet := ResolveAddrStr(conf.Local.IP, conf.Local.UDPPort)
	udpRetAddr, err := net.ResolveUDPAddr("udp4", wgStrRet)
	udpWGAddr, err := net.ResolveUDPAddr("udp4", wgStr)
	udpWGConn, err := net.DialUDP("udp4", udpRetAddr, udpWGAddr)
	if err != nil {
		return err
	}
	fmt.Printf("Dialed (UDP) %q \n", udpWGConn.RemoteAddr())
	defer udpWGConn.Close()

	// Loop waiting for VPN client connections to accept and handle
	for {
		conn, err := tcpPortListener.AcceptTCP()
		if err != nil {
			return err
		}

		go handleTCPtoClient_s(conn, udpWGConn)
		go handleClientToTCP_s(conn, udpWGConn)

	}
}
